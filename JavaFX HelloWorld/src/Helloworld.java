import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.stage.Stage; 
public class Helloworld extends Application {
	public static void main(String args[]) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) {
		Label label=new Label("Hello World");
		label.setPadding(new Insets(100,100,100,200));
		Scene scene=new Scene(label,500,300);
		primaryStage.setScene(scene);
		primaryStage.show();
		
	}
}
