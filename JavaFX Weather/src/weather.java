import java.io.IOException;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;
import javafx.stage.Window; 
public class weather extends Application{
	public static void main(String args[]) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		// TODO Auto-generated method stub
		primaryStage.setTitle("Weather Report");
		
		CategoryAxis xaxis=new CategoryAxis();
		xaxis.setLabel("Day of the week");
		
		NumberAxis yaxis=new NumberAxis();
		yaxis.setLabel("Temperature");
		
		
		BarChart barchart=new BarChart(xaxis, yaxis);
		XYChart.Series dataSeries1 =new XYChart.Series();
		
		dataSeries1.setName("Days");
		
		dataSeries1.getData().add(new XYChart.Data("Monday",28));
		dataSeries1.getData().add(new XYChart.Data("Tuesday",25));
		dataSeries1.getData().add(new XYChart.Data("Wednesday",23));
		dataSeries1.getData().add(new XYChart.Data("Thursday",19));
		dataSeries1.getData().add(new XYChart.Data("Friday",24));
		dataSeries1.getData().add(new XYChart.Data("Saturday",15));
		dataSeries1.getData().add(new XYChart.Data("Sunday",27));
		
		barchart.getData().add(dataSeries1);
		VBox vbox=new VBox(barchart);
		Scene scene=new Scene(vbox,800,800);
		
		primaryStage.setScene(scene);
		primaryStage.setHeight(700	);
		primaryStage.show();
		
		
		
		
			
		
		
	}
}
