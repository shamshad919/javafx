
import java.io.IOException;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;
import javafx.stage.Window; 
public class userdisplay extends Application {
	public static void main(String args[]) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws IOException {
		primaryStage.setTitle("User Details");
		GridPane gridpane=createRegistrationFormPane();
		
		Label title=new Label("User Details");
		title.setFont(Font.font("Arial", FontWeight.BOLD, 24));
		gridpane.add(title, 0,0,2,2);
		gridpane.setHalignment(title, HPos.CENTER);
		gridpane.setMargin(title, new Insets(100,300,450,200));
		
		Label name=new Label("Name:");
		gridpane.add(name,0,1);
		
		TextField namefield=new TextField();
		namefield.setPrefHeight(50);
		gridpane.add(namefield, 1, 1);
		
		Label age=new Label("Age:");
		gridpane.add(age,0,1);
		gridpane.setMargin(age,new Insets(150,0,0,0));
		
		TextField agefield=new TextField();
		agefield.setPrefHeight(50);
		gridpane.add(agefield, 1, 1);
		gridpane.setMargin(agefield, new Insets(150,0,0,0));
		
		Label color=new Label("Favourite Color");
		gridpane.add(color, 0, 1);
		gridpane.setMargin(color,new Insets(300,0,0,0));
		
		TextField colorfield=new TextField();
		colorfield.setPrefHeight(50);
		gridpane.add(colorfield, 1, 1);
		gridpane.setMargin(colorfield, new Insets(300,0,0,0));
		
		Button submitButton = new Button("Submit");
		submitButton.setPrefHeight(40);
	    submitButton.setDefaultButton(true);
	    submitButton.setPrefWidth(100);
	    gridpane.add(submitButton, 0, 2, 2, 1);
	    gridpane.setHalignment(submitButton, HPos.CENTER);
	    
	    submitButton.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				// TODO Auto-generated method stub
				if(namefield.getText().isEmpty()) {
					showAlert(Alert.AlertType.ERROR,gridpane.getScene().getWindow(),"","Please Enter the Name");
					return;
				}
				if(agefield.getText().isEmpty()) {
					showAlert(Alert.AlertType.ERROR,gridpane.getScene().getWindow(),"","Please Enter the Age");
					return;
				}
				if(colorfield.getText().isEmpty()) {
					showAlert(Alert.AlertType.ERROR,gridpane.getScene().getWindow(),"","Please Enter the Color");
					return;
				}
				GridPane gridpane1=createRegistrationFormPane();
				String s="Hello"+" "+namefield.getText()+"!!";
				Label res=new Label(s);
				String b="You are "+agefield.getText()+" years old and your favourite colour is "+colorfield.getText();
				Label ag=new Label(b);
				gridpane1.add(res, 1, 1);
				gridpane1.add(ag, 1, 2);
				Scene scene1=new Scene(gridpane1,500,500);
				primaryStage.setScene(scene1);
				primaryStage.show();
			}
		});
		
		Scene scene=new Scene(gridpane,800,800);
		primaryStage.setScene(scene);
		primaryStage.show();
		
	}
	private void showAlert(Alert.AlertType alertType, Window owner, String title, String message) {
        Alert alert = new Alert(alertType);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.initOwner(owner);
        alert.show();
    }

	private GridPane createRegistrationFormPane() {
		GridPane gridpane=new GridPane();
		gridpane.setAlignment(Pos.CENTER);
		gridpane.setPadding(new Insets(40,40,40,40));
		gridpane.setHgap(5);
		gridpane.setVgap(5);

        return gridpane;
	}
}
